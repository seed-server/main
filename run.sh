source "$(dirname $0)/settings.sh"

case $1 in

    -x)
        shift 1
        $@
    ;;
    *)
        SCRIPT="$1"
        if [ -f "$SCRIPT" ]
        then
            source "$SCRIPT"
        fi
    ;;
esac

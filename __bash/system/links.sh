#------------------------------------------------
function LINKS_INIT(){
#------------------------------------------------
# décrit un composant
#
#------------------------------------------------
    #echo $LINKS_PATH
    if [ -z "$1" ]
    then
        export LINKS_PATH="$(realpath .)"
    else
        export LINKS_PATH="$1"
    fi


}
#------------------------------------------------
function LINKS_RM(){
#------------------------------------------------
    COMMAND unlink "$LINKS_PATH/$1"
}
#------------------------------------------------
function LINKS_ADD(){
#------------------------------------------------

    local source="$(realpath $1)"
    local target="$LINKS_PATH/$2"
    #echo $source "      ---        " $target

    if [ ! -e "$source" ]
    then
        ERROR "no source $source"
        exit 1

    elif [ -e "$target" ]
    then
        MESSAGE "X target exists $target"
        #exit
    else

        dirname="$(dirname $target)"

        if [ ! -d "$dirname" ]
        then
            a=1
            COMMAND mkdir -p "$dirname"
        fi

        COMMAND ln -s "$source" "$target"
    fi
}
#------------------------------------------------
function LINKS_ADD_STORE(){
#------------------------------------------------
    local source="$(realpath $1)"
    local target="$2"


    for elt in $(find "$source" -mindepth 1 -maxdepth 1 -type d)
    do
        name="$(basename $elt)"
        LINKS_ADD "$elt" "$target/$name"
    done

}
#------------------------------------------------
function LINKS_ADD_FILTER_STORE(){
#------------------------------------------------

    local source="$1"
    local filter="$2"
    local target="$3"

    for elt in $(find $source -type d -name "$filter" )
    do
        LINKS_ADD_STORE $elt $target

    done
}
#------------------------------------------------
function LINKS_ADD_FILTER_DIRS(){
#------------------------------------------------

    local source="$1"
    local filter="$2"
    local target="$3"

    for elt in $(find $source -type d -name "$filter" )
    do
    LINKS_ADD $elt $target/$(basename $(dirname $elt))

    done
}
#------------------------------------------------

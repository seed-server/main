#------------------------------------------------
function SCRIPT(){
#------------------------------------------------
# décrit un composant
#
#------------------------------------------------
        script="$1"
        export APP_PATH="$(dirname $script)"
        PARAGRAPH "$script"
        #cat "$script"
        source "$script"


}
#------------------------------------------------
function SYSTEM_SCRIPT(){
#------------------------------------------------
# pour toutes les appli ,execute le script passé en argument
#
#------------------------------------------------


    for script in $(find $SEED_DIR -mindepth 2 -type f -name "$1" | sort -V)
    do
        SCRIPT "$script"
    done


}

#------------------------------------------------
function SYSTEM_CONFIG(){
#------------------------------------------------

    COMMAND mkdir -p "$SEED_ENV/libs"
    LINKS_INIT "$SEED_ENV/libs"

    PARAGRAPH scripts
    MERGE_SCRIPTS $SEED_DIR "__install__.sh" > "$SEED_ENV/libs/__install__.sh"
    MERGE_SCRIPTS $SEED_DIR "__config__.sh" > "$SEED_ENV/libs/__config__.sh"
    MERGE_SCRIPTS $SEED_DIR "__settings__.sh" > "$SEED_ENV/libs/__settings__.sh"

    PARAGRAPH system
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__bin" "__bin"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__etc" "__etc"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__static" "__static"


    PARAGRAPH languages
    LINKS_ADD_FILTER_STORE $SEED_DIR "__python3" "__python3"
    LINKS_ADD_FILTER_STORE $SEED_DIR "__php" "__php"
    LINKS_ADD_FILTER_STORE $SEED_DIR "__js" "__js"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__bash" "__bash"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__godot" "__godot"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__src" "__arduino"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__jinja2" "__jinja"

    PARAGRAPH dev
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__tests" "__tests"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__sphinx" "__sphinx"

    PARAGRAPH plugins
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__www" "__www"
    LINKS_ADD_FILTER_DIRS $SEED_DIR "__mods" "__mods"


}
#------------------------------------------------

